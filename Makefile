CFLAGS = -D_GNU_SOURCE -std=c11 -pedantic -Wall -Wvla -Werror

# PGM = compile

# all: clear $(PGM)
# compile: compile.c

# clear:
# 	clear

# clean:
# 	rm -f $(PGM)
# 	rm -f hello
# 	rm -f res_compile.txt

all: progs client gstat maint 

# Progs.c (serveur)
progs: progs.o utils.o 
	cc $(CFLAGS) -o progs progs.o utils.o

progs.o: progs.c utils.h server_request.h repExec.h ipc.h socketInfo.h
	cc $(CFLAGS) -c progs.c

# Client.c 
client: client.o utils.o
	cc $(CFLAGS) -o client client.o utils.o

client.o: client.c socketInfo.h server_request.h utils.h messages.h
	cc $(CFLAGS) -c client.c

# gstat 
gstat: gstat.o utils.o
	cc $(CFLAGS) -o gstat gstat.o utils.o

gstat.o: gstat.c utils.h repExec.h ipc.h
	cc $(CFLAGS) -c gstat.c

# maint
maint: maint.o utils.o
	cc $(CFLAGS) -o maint maint.o utils.o

maint.o: maint.c utils.h repExec.h ipc.h
	cc $(CFLAGS) -c maint.c

utils.o: utils.h utils.c
	cc $(CFLAGS) -c utils.c

clean:
	rm *.o
	rm progs
	rm client
	rm gstat
	rm maint


