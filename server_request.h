#define TAILLE 256
/* Functionality codes */
#define ADD_PROG -1
#define EXEC_PROG -2
#define REQUEST_SUCCESFUL 10

typedef struct {
    char message[TAILLE];
	int code;
	int number_char; //nb char file name
    char file_name[TAILLE]; //file's name
    int num_prog; // prog's number
    int etat; 
    int temps_exec;
    int code_retour;
} structRequest;