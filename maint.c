#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "repExec.h"
#include "ipc.h"
#define MAX 1000

int main(int argc, char** argv) {

    int choix = atoi(argv[1]);

    switch(choix) {
        case 1:
            init_shm(sizeof(int)+(sizeof(structRepertExec)*MAX)); //on réserve la place pour 1 int (taille logique) et MAX * struct
            init_sem(1);
            break;
        case 2: 
           del_shm(sizeof(int)+(sizeof(structRepertExec)*MAX));
           del_sem();
            break;
        case 3: 
            // A tester avec le repertoire stat et serveur
            down(); // bloque l'accès à la ressource
            sleep(atoi(argv[2]));
            up(); // libère l'accès à la ressource
            break;
    }
}