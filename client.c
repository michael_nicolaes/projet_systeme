#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include "socketInfo.h"
#include "messages.h"
#include "server_request.h"
#include "repExec.h"
#include "utils.h"
#define TAILLE_RECURRENT 100
#define PERMISSION 0644


char* server_ip;
int server_port;
int delay;
int sockfd;
int retour;
int running;
int num_prog[TAILLE_RECURRENT];
int index_recurrent = 0;


/*
Reads the server's answer on the given socket
First reads a structRepertExec then acts accordingly to the state of the response
*/
void read_response(int source_socket){
		structRepertExec response = {0};
		retour = read(source_socket, &response, sizeof(structRepertExec));
		checkCond(retour != sizeof(structRepertExec), "Error reading response *");
		if(response.etat == WRONG_NUM){
			char* bad_number = "Invalid program number\n";
			retour = write(0, bad_number, strlen(bad_number));
			checkCond(retour != strlen(bad_number), "Error printing wrong_num");

		}else if(response.etat == COMPILE_ERROR){
			char* bad_number = "Compile error in program\n";
			retour = write(0, bad_number, strlen(bad_number));
			checkCond(retour != strlen(bad_number), "Error printing compile error");
		}else if(response.etat == PROG_BAD_ENDING){
			printf("Program execution error\n");
			
		}else if(response.etat == ALL_OK){
			printf("Program execution was successful !\n");
			printf("Execution time = %ld \n", response.nbrMicro);
			printf("Return code : %d \n", response.exec_return_code);
			printf("Execution output : \n");
			
			char buffer_output[TAILLE];
			int nb = 0;

			while((nb = read(source_socket, buffer_output, TAILLE)) != 0){
				retour = write(0, buffer_output, nb);
				checkCond(retour != nb, "Error printing execution results");
			}
			printf("\n");
		}else{
			perror("Not possible");
		}
		int ret = close(source_socket);
		checkNeg(ret, "Error closing socket reading response");
}


/*
Open TCP connection with server and closes it
Send exec request for every program's num in progs
Prints return from server
*/
int exec_prog(int total_prog, int progs[]){
	
	for(int i = 0; i < total_prog; i++){
		int socket = initSocketClient(server_ip, server_port);
		structRequest request = {0};
		request.code = EXEC_PROG;
		request.num_prog = progs[i];
		
		//sending request
		retour = write(socket, &request, sizeof(structRequest));
		checkCond(retour != sizeof(structRequest), "Error sending request in recurrent");

		retour = shutdown(socket, SHUT_WR);
		checkNeg(retour, "Error shutdown +");

		//READ answer with all info
		sleep(delay/2);
		printf("**** Recurrent Execution : program number %d ******\n", progs[i]);
		read_response(socket);
		printf("***************************************************\n");
	}
	return 1;	
}

int main(int argc, char** argv)
{
    int tailleReponse;
	char choix;
    char bufferRead[TAILLE];  
	int pid_1;
	int pid_2;
	int pipefd[2];

	structMessage msg;
	
	if(argc != 4){
		printf("Bad use : we need 3 args (ip, port, delay)\n");
		exit(0);
	}

	server_ip = argv[1];
	server_port = atoi(argv[2]);
	delay = atoi(argv[3]);
    
	//creating childs and pipe

	retour = pipe(pipefd);
	checkNeg(retour, "Erreur pipe 1");
	pid_1 = fork();
	checkNeg(pid_1, "Erreur fork 1");

	if(pid_1){
		//parent : terminal
		running = 1;
		pid_2 = fork();
		checkNeg(pid_2, "Erreur fork 2");	

		if(!pid_2){
			//child 2 : reccurent
			close(pipefd[1]); //closing writing pipe
			
			structMessage message_recu = {0};

			while(read(pipefd[0], &message_recu, sizeof(structMessage)) == sizeof(structMessage)){
				if(message_recu.auteur == TERMINAL){
					
					if(index_recurrent == 99){
						index_recurrent = 0;
					} 
					num_prog[index_recurrent] = message_recu.numProg;
					index_recurrent++;
				}else if(message_recu.auteur == MINUTERIE){
					exec_prog(index_recurrent, num_prog);
				}else{
					perror("Message inconnu");
				}
				
			}
			close(pipefd[0]);
		}

		close(pipefd[0]); //closing parent reading pipe 
		
		//treating user's response
		while(running){
			retour = write(0, "Enter command (+/*/@/q) :", strlen("Enter command (+/*/@/q) :"));
			checkNeg(retour, "Error write 1");
			tailleReponse = read(0, bufferRead, TAILLE);
			checkNeg(tailleReponse, "Error read 1");

			//replace \n by \0
			bufferRead[tailleReponse-1] = '\0';
			choix = bufferRead[0];

			if(choix == '+'){
				//connecting TCP 
				int socket_add = initSocketClient(server_ip, server_port);
				//start of file path in bufferRead+(2*sizeof(char))
				structRequest request = {0};
				strcpy(request.file_name, bufferRead+(2*sizeof(char)));
			
				request.code = ADD_PROG;
				request.number_char = strlen(request.file_name);
				
				//opening file
				int fd_file;
				fd_file = open(request.file_name, O_RDONLY, PERMISSION);
				checkNeg(fd_file, "Erreur ouverture fichier !");

				//sending request to server
				retour = write(socket_add, &request, sizeof(structRequest));
				checkCond(retour!=sizeof(structRequest), "Error sending request +");

				//reading file and streaming it to the server
				char bufferFile[TAILLE];
				int nbChar = 0;
				
				while((nbChar = read(fd_file, &bufferFile, TAILLE)) != 0){
					write(socket_add, bufferFile, nbChar);
				}
				close(fd_file);
				
				//shutdown
				retour = shutdown(socket_add, SHUT_WR);
				checkNeg(retour, "Error shutdown +");

				//read server's answer

				//1- the number 
				int prog_number;
				retour = read(socket_add, &prog_number, sizeof(int));
				checkCond(retour != sizeof(int), "Error reading server response number");
				
				retour = write(0, &prog_number, sizeof(int));
				checkCond(retour != sizeof(int), "Error printing number to user");
				//2- the compile results
				char buffer_answer[TAILLE];
				while((nbChar = read(socket_add, &buffer_answer, TAILLE)) != 0){
					write(0, buffer_answer, nbChar);
				}

				close(socket_add);
			}else if(choix == '*'){
				int num = atoi(bufferRead+(2*sizeof(char))); //prog num is at this position
				msg.numProg = num; 
				msg.auteur = TERMINAL;
				retour = write(pipefd[1], &msg, sizeof(structMessage));
				checkCond(retour!=sizeof(structMessage), "Error write * choice");

			}else if(choix == '@'){
				//send exec request
				structRequest request_exec;
				request_exec.code = EXEC_PROG;
				int num =atoi(bufferRead+(2*sizeof(char)));
				request_exec.num_prog = num;
				
				int socket_exec = initSocketClient(server_ip, server_port);
				retour = write(socket_exec, &request_exec, sizeof(structRequest));
				
				checkCond(retour!=sizeof(structRequest), "Error sending exec request");
				
				retour = shutdown(socket_exec, SHUT_WR);
				checkNeg(retour, "Error shutdown @");

				//READ answer with all info
				printf("**** Normal Execution : program number %d ******\n", num);
				read_response(socket_exec);
				printf("***************************************************\n");
				

			}else if(choix == 'q'){
				running = 0;
				kill(pid_1, SIGKILL);
				kill(pid_2, SIGKILL);
				close(pipefd[1]);
			}else{
				printf("Not a valid command, exiting... \n");
				exit(0);
			}
		}
		

	}else{
		//child 1 : minuterie
		close(pipefd[0]); //closing minuterie reading pipe
		structMessage mess;
		mess.auteur = MINUTERIE;
		
		while(1){
			sleep(delay);
			retour = write(pipefd[1], &mess, sizeof(structMessage));
			checkNeg(retour, "Erreur write minuterie");
		}
		close(pipefd[1]);
	}

	
}




