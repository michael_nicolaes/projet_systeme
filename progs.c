#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "server_request.h"
#include "repExec.h"
#include "utils.h"
#include "ipc.h"
#include "socketInfo.h"



#define TAILLE_TEXTE 256
#define CODE_RETOUR_OK 1 // 1 si terminé normalement
#define CODE_RETOUR_KO 0 // 0 si pas terminé normalement
#define MAX 1000

int running = 1;
int pid_exec;
char name_compile[TAILLE];
char name_prog[TAILLE];
char file_compile[TAILLE];

long now() {
  struct timeval  tv;
  
  int res = gettimeofday(&tv, NULL);
  checkNeg(res, "Error gettimeofday");
  
  return tv.tv_sec * 1000000 + tv.tv_usec;
}

void handler1() {
    execl("/usr/bin/gcc", "gcc", "-o", name_compile, name_prog, NULL);
    perror("Error execl 1");
}

void handler2() {
    execl("/bin/mv", "mv", name_compile, "./rep_code", NULL);
    perror("Error execl 2");
}

void handler3(void* arg1, void* arg2) {
    execl((char*)arg1, (char*)arg2, 0);
    perror("Error execl 3");
}


int main (int argc, char **argv) {
    
   

    int sockfd;
    int newsockfd;
    int ret;
    char rep_path[TAILLE];
    
    structRequest request; // message envoyé par le client
    int port = atoi(argv[1]);

    sockfd = initSocketServer(port); // ouvrir une connexion TCP
    printf("Le serveur tourne sur le port : %i \n", port);

    void* shared_mem;
    shared_mem = init_shm(sizeof(int)+(sizeof(structRepertExec)*MAX));           
    //première struct se trouve en shared_mem+1
    int* taille_logique = (int*)(shared_mem);
    structRepertExec* first_struct = (structRepertExec*)shared_mem+sizeof(int);


   
    while(running){
        
        newsockfd = accept(sockfd, NULL, NULL); // récupere l'adresse du client se connectant

        if(newsockfd > 0) {
            ret = read(newsockfd, &request, sizeof(structRequest)); // on lit ce que le client écrit
            checkCond(ret!=sizeof(structRequest), "Error reading request -1");
                    
            pid_exec = fork();
            
            if(!pid_exec){
                //on crée un fils pour traiter la demande
                /* Si client veut ajouter un programme --> -1 */
                    
                    if(request.code == ADD_PROG) { 
    
                        // reading the file contents
                        down();

                        char* file_name = request.file_name;
                        char* file_path = "rep_code/";
                        strcpy(rep_path, file_path);
                        strcat(rep_path, file_name);

                        printf(" final final %s\n", rep_path);
                        
                        //creating file to put reading content
                        int fd_file = open(rep_path, O_RDWR | O_CREAT | O_TRUNC, 0644);
                        checkNeg(fd_file, "Error creating file in code -1");
                        char buffer_file[TAILLE];
                        int nb_read = 0;
                        while((nb_read =read(newsockfd, buffer_file, TAILLE)) != 0){
                            ret = write(fd_file, buffer_file, nb_read);
                            checkCond(ret!=nb_read, "Erreur copie fichier en repertoire de code");
                        }
                        // envoyer la réponse du serveur au client 
                        // on va placer la structure et rechercher la taille logique
                        structRepertExec* rep_nouveau = (structRepertExec*)first_struct+(*taille_logique*sizeof(structRepertExec));
                        
                        rep_nouveau->numProg = (*taille_logique);
                        strcpy(rep_nouveau->nom, rep_path);
                        
                        char retour_user[TAILLE];
                        ret = sprintf(retour_user, "Program number : %d \n", rep_nouveau->numProg);
                        checkNeg(ret, "Error sprintf");

                        ret = write(newsockfd, &retour_user, strlen(retour_user));
                        checkCond(ret != strlen(retour_user), "Error sending num");

                        //compiling file into another file
                        //1- creating file for errors
                        int fd_error = open("./rep_code/res_compile.txt", O_CREAT | O_RDWR| O_TRUNC, 0644);
                        checkNeg(fd_error, "ERROR open");

                        int stderr_copy = dup(2);
                        checkNeg(stderr_copy, "ERROR dup");
                        
                        int ret = dup2(fd_error, 2);
                        checkNeg(ret, "ERROR dup2");

                        //2- compiling c
                        strcpy(name_prog, request.file_name);
                        strncpy(name_compile, request.file_name, strlen(request.file_name)-2);
                        
                        fork_and_run(handler1);
                        int status;
                        wait(&status);
                        
                        //moving the file into the rep_code
                        fork_and_run(handler2);
                        wait(NULL);
                        
                        ret = dup2(stderr_copy, 2);
                        checkNeg(ret, "ERROR dup");

                        if(WEXITSTATUS(status) == 0){
                            //Read in compile error file and printing it to the client
                            int number_read;
                            int read_or_not = 0;
                            char error_buffer[TAILLE];
                            
                            //Opening file to read it
                            int fd_error2 = open("./rep_code/res_compile.txt", O_RDONLY, 0644);
                            checkNeg(fd_error2, "Error opening compile res");

                            while((number_read = read(fd_error2, error_buffer, TAILLE)) != 0){
                                
                                checkNeg(number_read, "Error reading compile file");
                                ret = write(newsockfd, error_buffer, number_read);
                                checkCond(ret != number_read, "Error printing compile to user");
                                read_or_not = 1;
                            }
                            if(read_or_not == 1){
                                    rep_nouveau->etat = COMPILE_ERROR;
                            }

                            *taille_logique = *taille_logique + 1;
                            
                            close(fd_error2);
                            
                            ret = shutdown(newsockfd, SHUT_WR);
				            checkNeg(ret, "Error shutdown ");
                        }

                        close(stderr_copy);
                        close(fd_error);
                        close(newsockfd);
                        up();
                    }

                    // FONCTION @ Commencer ici
                    if(request.code == EXEC_PROG) { 
                        down();
                    
                        // On lit les infos envoyés par le client
                        ret = read(newsockfd, &request, sizeof(structRequest));
                     
                        structRepertExec response = {0}; //answer for the client

                        if(request.num_prog < 0 || request.num_prog >= (*taille_logique)){
                            //Bad prog number
                            
                            response.etat = WRONG_NUM;
                            ret = write(newsockfd, &response, sizeof(structRepertExec));
                            checkCond(ret != sizeof(structRepertExec), "Error sending bad num prog");

                            ret = shutdown(newsockfd, SHUT_WR);
				            checkNeg(ret, "Error shutdown ");
                            close(newsockfd);
                            up();
                            exit(0);
                        }

                        //1-Get the compiled C file linked with the number
                        
                        structRepertExec* number_struct = (structRepertExec*)first_struct+(request.num_prog*sizeof(structRepertExec));
                        
                        if(number_struct->etat == COMPILE_ERROR){
                            //The file linked to this number didn't compile when added
                            
                            response.etat = COMPILE_ERROR;
                            ret = write(newsockfd, &response, sizeof(structRepertExec));
                            checkCond(ret != sizeof(structRepertExec), "Error prog compile");
                            
                            ret = shutdown(newsockfd, SHUT_WR);
				            checkNeg(ret, "Error shutdown ");
                            
                            close(newsockfd);
                            up();
                            exit(1);
                        }

                        strncpy(file_compile, number_struct->nom, strlen(number_struct->nom)-2);
                        file_compile[strlen(number_struct->nom)-1] = '\0';
                       
                        char name_local[TAILLE];
                        strncpy(name_local, number_struct->nom, strlen(number_struct->nom)-2);
                        name_local[strlen(number_struct->nom)-1] = '\0';
                       
                        char arg1[TAILLE];
                        char arg2[TAILLE];
                        //2-Create a file to receive the output of the execution
                        char new_path[TAILLE];
                        char new_name[TAILLE];
                        strcpy(new_name, name_local);
                        strcpy(arg1, new_name);
                        strcat(new_name, "_execution");

                        sprintf(new_path, "./%s", new_name);
                        new_path[strlen(new_path)] = '\0';


                        //creating arg2 for handler exec file
                        int j = 0;
                        int copying = 0;
                        for(int i = 0; i < strlen(arg1)-1; i++){
                            if(arg1[i] == '/'){
                                copying = 1;
                            }
                            if(copying){
                                arg2[j] = arg1[i+1];
                                j++;
                            }
                        }
                        strcat(arg2, ".c\0");

                        //int fd_execution = open("./rep_code/res_execution.txt", O_CREAT | O_RDWR| O_TRUNC, 0644);
                        int fd_execution = open(new_path, O_CREAT | O_RDWR| O_TRUNC, 0644);
                        checkNeg(fd_execution, "ERROR open execution !!");

                        int stdout_copy = dup(1);
                        checkNeg(stdout_copy, "ERROR dup stdout");
                         
                        int ret = dup2(fd_execution, 1);  // on redirige la sortie standard(1) vers le fichier 
                        checkNeg(ret, "ERROR dup2");

                        //3-Execute the file and put results in the right file
                        long t1 = now();
                        //fork_and_run(handler3);
                        fork_and_run2(handler3, arg1, arg2);
                        int status;
                        wait(&status);
                        long t2 = now();
                        long exec_time = t2 - t1;

                        close(fd_execution);
                        ret = dup2(stdout_copy, 1);
                        checkNeg(ret, "ERROR dup");
                        
                        //4-Update the struct in the shmem accordingly
                        //5-Return information to client
                        if(WIFEXITED(status)){
                            //4- updating shmem only if the execution is OK
                            long exec_time_prog = exec_time;
                            exec_time += number_struct->nbrMicro; 
                            number_struct->etat = ALL_OK;
                            number_struct->nbrExecutions++;
                            number_struct->nbrMicro = exec_time; 
                            number_struct->exec_return_code = WEXITSTATUS(status);
                            
                            //execution OK
                            //printing execution results to client
                            int nb_read = 0;
                            char buffer_exec[TAILLE];
                            response.etat = ALL_OK;
                            response.exec_return_code = WEXITSTATUS(status);
                            response.nbrMicro = exec_time_prog; //exec_time
                            ret = write(newsockfd, &response, sizeof(structRepertExec));
                            checkCond(ret != sizeof(structRepertExec), "Error printing struc response to client");

                            int fd_execution2 = open(new_path, O_RDONLY, 0644);
                            //int fd_execution2 = open("./rep_code/res_execution.txt", O_RDONLY, 0644);
                            checkNeg(fd_execution2, "Error opening execution res");

                            while((nb_read = read(fd_execution2, buffer_exec, TAILLE)) != 0){
                                ret = write(newsockfd, buffer_exec, nb_read);
                                checkCond(ret != nb_read, "Error writing exec results to client");
                            }
                            ret = shutdown(newsockfd, SHUT_WR);
				            checkNeg(ret, "Error shutdown ");
                            
                            close(fd_execution2);
                            close(newsockfd);
                            
                        }else{
                            //execution KO
                            response.etat = PROG_BAD_ENDING;
                            ret = write(newsockfd, &response, sizeof(structRepertExec));
                            checkCond(ret != sizeof(structRepertExec), "Error printing struc response to client");
                        }
                        close(newsockfd); 
                        up(); 
                    }
                }
            }
    }
    ret = shmdt(shared_mem);
    checkNeg(ret, "Error shmdt in add file");
    close(sockfd); // fermer connexion TCP
} 
