#include <stdlib.h>
#include <stdio.h>

#include "repExec.h"
#include "ipc.h"

int main(int argc, char** argv) {

    int num = atoi(argv[1]);

    down(); //sem getting into shmem // d'abord down() car sem initialisé à 1

    void* shared_mem;
    shared_mem = init_shm(sizeof(int)+(sizeof(structRepertExec)*1000));           
    //première struct se trouve en shared_mem+1
    structRepertExec* first_struct = (structRepertExec*)shared_mem+sizeof(int);
    structRepertExec* rep = (structRepertExec*)first_struct+(num*sizeof(structRepertExec));

    rep->numProg = num;
    printf("Voici les statistiques : \n");

    printf("Num du programme %d\n", rep->numProg);
    printf("Nom du fichier source : %s\n", rep->nom);

    printf("etat %d\n", rep->etat);

    printf("Nombre d'executions : %d\n", rep->nbrExecutions);
    printf("Nombre de microsecondes : %lu\n", rep->nbrMicro);

    up();
}