/* Used by server and client */
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
/* struct sockaddr_in */
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"

#define TAILLE 256
#define SERVER 16

//#define SERVER_PORT 9501
//#define SERVER_IP	"127.0.0.1"  /* localhost */

/* return sockfd */
int initSocketServer(int port)
{
	
	int ret; int sockfd;
	struct sockaddr_in addr;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	checkNeg(sockfd, "Erreur initSocketServer");
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);	
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	ret = bind(sockfd, (struct sockaddr*) &addr, sizeof(addr));
	checkNeg(ret, "Erreur bind");
	listen(sockfd, TAILLE);
	/*
	while(1){
		newsockfd = accept(sockfd, NULL, NULL);
		checkNeg(newsockfd, "Erreur accept server");
		ret = read(newsockfd, &msg, sizeof(msg));
		checkNeg(ret, "Erreur read server");
	}
	*/
	return sockfd;
}

int initSocketClient(char ServerIP[SERVER], int Serverport)
{
  	
  	int ret;
  	int sockfd;
  	struct sockaddr_in addr;
  	sockfd = socket(AF_INET, SOCK_STREAM, 0);
  	checkNeg(sockfd, "Erreur socket client");
  	memset(&addr, 0, sizeof(addr));
  	addr.sin_family = AF_INET;
  	addr.sin_port = htons(Serverport);
  	inet_aton(ServerIP, &addr.sin_addr);
  	ret = connect (sockfd, (struct sockaddr*) &addr, sizeof(addr));
  	checkNeg(ret, "Erreur connect");
  	return sockfd;
}
