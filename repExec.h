#include <stdbool.h>

#define SIZE_PROG 256
#define WRONG_NUM -2
#define COMPILE_ERROR -1
#define PROG_BAD_ENDING 0
#define ALL_OK 1
#define TAILLE 256
/* struct pour les ressources partagées */

typedef struct {
    int numProg;    //num programme
    char nom[TAILLE];
    int exec_return_code;
    int nbrExecutions; // nbr d'executions du programme
    long nbrMicro; // nbr de microsecondes cumulées;
    int etat; //-2 prog existe pas, -1 compile pas, 0 pas terminé normalement, 1 terminé normalement
    int nbrProgramme; // retient le nbr de programme contenu dans repertExec
} structRepertExec;